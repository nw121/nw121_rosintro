#!/usr/bin/env python

from __future__ import print_function
from six.moves import input
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
from math import pi

class MidtermMoveitInterface(object):
    """MidtermMoveitInterface"""
    # Main class and object to interface with the robot

    def __init__(self):
        super(MidtermMoveitInterface, self).__init__()

        ## Initialize `moveit_commander`_ and a `rospy`_ node
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("nw121_moveit_midterm_script", anonymous=True)

        ## Instantiate a `RobotCommander`_ object to provide information such as the robot's kinematic model and the robot's current joint states
        robot = moveit_commander.RobotCommander()

        ## Instantiate a `PlanningSceneInterface`_ object to provide a remote interface for getting, setting, and updating the robot's internal understanding of the surrounding world:
        scene = moveit_commander.PlanningSceneInterface()

        ## Instantiate a `MoveGroupCommander`_ object to interface can be used to plan and execute motions
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        ## Create a `DisplayTrajectory`_ ROS publisher which is used to display trajectories in Rviz
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        # Setting class variables
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = move_group.get_planning_frame()
        self.eef_link = move_group.get_end_effector_link()
        self.group_names = robot.get_group_names()
    
    def initial_position(self):
        # This function sets the robot to the initial position for drawing
        # This sets the arm in a position where it is facing the -z direction and keeps the end effector close to z = 0

        move_group = self.move_group
        # Set the pose to the current pose
        joint_goal = move_group.get_current_joint_values()

        # Set the joint angles
        joint_goal[0] = -pi
        joint_goal[1] = -(3/4)*pi
        joint_goal[2] = -(1/2)*pi
        joint_goal[3] = (7/4)*pi 
        joint_goal[4] = -1.5*pi
        joint_goal[5] = 1.5*pi

        # Move to those angles using any path within workspace
        move_group.go(joint_goal, wait=True)

        print("Current position values: ", move_group.get_current_pose().pose)
        print("Current joint values: ", move_group.get_current_joint_values())

    def draw_N(self):
        # This is the function responsible for drawing the N
        # It assumes that the robot is in the initial position
        # It uses helper function move_execute_cartesian to move to each point

        move_group = self.move_group
        # Set the pose to the current pose
        wpose = move_group.get_current_pose().pose
        
        # Point 1
        self.move_execute_cartesian(move_group, wpose, y_pos=0.4)

        # Point 2
        self.move_execute_cartesian(move_group, wpose, x_pos=0.15, y_pos=-0.4)

        # Point 3
        self.move_execute_cartesian(move_group, wpose, y_pos=0.4)

        # End sequence (get back to initial position and clear targets)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()
    
    def draw_I(self):
        # This is the function responsible for drawing the I
        # It assumes that the robot is in the initial position
        # It uses helper function move_execute_cartesian to move to each point
        move_group = self.move_group
        # Set the pose to the current pose
        wpose = move_group.get_current_pose().pose

        # Point 1
        self.move_execute_cartesian(move_group, wpose, y_pos=0.4)

        # End sequence (get back to initial position and clear targets)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()

    def draw_W(self):
        # This is the function responsible for drawing the W
        # It assumes that the robot is in the initial position
        # It uses helper function move_execute_cartesian to move to each point

        move_group = self.move_group
        wpose = move_group.get_current_pose().pose

        # Point 1
        self.move_execute_cartesian(move_group, wpose, x_pos=-0.07, y_pos=0.4)

        # Point 2
        self.move_execute_cartesian(move_group, wpose, x_pos=0.07, y_pos=-0.4)

        # Point 3
        self.move_execute_cartesian(move_group, wpose, x_pos=0.07, y_pos=0.2)

        # Point 4
        self.move_execute_cartesian(move_group, wpose, x_pos=0.07, y_pos=-0.2)

        # Point 5
        self.move_execute_cartesian(move_group, wpose, x_pos=0.07, y_pos=0.4)

        # End sequence (get back to initial position and clear targets)
        success = move_group.go(wait=True)
        move_group.stop()
        move_group.clear_pose_targets()


    def move_execute_cartesian(self, move_group, wpose, x_pos=0, y_pos=0, z_pos=0):
        # This function takes in an offset in the direction you want to move the EE
        # It then moves the EE to that position
        waypoints = []
        wpose.position.x += x_pos
        wpose.position.y += y_pos
        wpose.position.z += z_pos

        # Compute a sequence of waypoints that make the end-effector move in straight line segments that follow the poses specified as waypoints.
        # Configurations are computed for every 0.01 meters; 
        # The return value is a tuple: a fraction of how much of the path was followed, the actual RobotTrajectory. 
        waypoints.append(copy.deepcopy(wpose))
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )

        # Execute the trajectory
        move_group.execute(plan, wait=True)

def main():
    try:
        print("")
        print("----------------------------------------------------------")
        print("This is Nick Ward's ECE383 Midterm Project")
        print("----------------------------------------------------------")
        print("Press Ctrl-D to exit at any time")
        print("")
        input(
            "============ Press `Enter` to set up the moveit_commander ..."
        )
        move_it = MidtermMoveitInterface()

        input(
            "============ Press `Enter` to get to starting position ..."
        )
        move_it.initial_position()

        input(
            "============ Press `Enter` to get to draw an N..."
        )
        move_it.draw_N()

        input(
            "============ Press `Enter` to get to draw an I..."
        )
        move_it.draw_I()

        input(
            "============ Press `Enter` to get to draw a W..."
        )
        move_it.draw_W()

        print("============ Everything successfully executed!")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()