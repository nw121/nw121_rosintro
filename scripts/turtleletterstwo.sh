#!/usr/bin/bash
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- ' [0.0 , 2.0 , 0.0] ' ' [0.0 , 0.0 , 0.0] '
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- ' [1.0 , -2.0 , 0.0] ' ' [0.0 , 0.0 , 0.0] '
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- ' [0.0 , 2.0 , 0.0] ' ' [0.0 , 0.0 , 0.0] '
rosservice call /spawn 7.3 5.5 0 turtle2
rosservice call /turtle2/set_pen "{r: 255, g: 0, b: 0, width: 2, 'off': 0}"
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
-- ' [0.0 , 1.0 , 0.0] ' ' [0.0 , 0.0 , 0.0] '
rosservice call /turtle2/set_pen "{r: 255, g: 0, b: 0, width: 2, 'off': 1}"
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
-- ' [0.0 , 0.3 , 0.0] ' ' [0.0 , 0.0 , 0.0] '
rosservice call /turtle2/set_pen "{r: 255, g: 0, b: 0, width: 2, 'off': 0}"
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
-- ' [0.0 , 0.1 , 0.0] ' ' [0.0 , 0.0 , 0.0] '
rosservice call kill turtle1
rosservice call kill turtle2
